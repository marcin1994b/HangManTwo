package com.marcinbaranski.hangmantwo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.marcinbaranski.hangmantwo.R;
import com.marcinbaranski.hangmantwo.presenters.AuthorizationContract;
import com.marcinbaranski.hangmantwo.presenters.AuthorizationPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignInActivity extends AppCompatActivity implements AuthorizationContract.PublishToView{

    private static final String TAG = "SignInActivity";


    @BindView(R.id.editTextEmail)
    EditText emailEditText;
    @BindView(R.id.editTextPassword)
    EditText passwordEdtiText;
    @BindView(R.id.ButtonSignIn)
    Button signInButton;
    @BindView(R.id.TextViewRememberPassword)
    TextView rememberPasswordTextView;
    @BindView(R.id.textViewSingUp)
    TextView signUpTextView;

    @OnClick({R.id.textViewSingUp, R.id.TextViewRememberPassword, R.id.ButtonSignIn })
    public void onClick(View v) {
        if(v == signInButton){
            Log.i(TAG, "Click signIn button.");
            forwardInteraction.onSignInButtonClick(this, emailEditText.getText().toString().trim(),
                    passwordEdtiText.getText().toString().trim());
        }else if(v == rememberPasswordTextView){
            Log.i(TAG, "start RememberPasswordActivity.");
            Intent intent = new Intent(this, RemindPasswordActivity.class);
            startActivity(intent);
        }else if(v == signUpTextView){
            Log.i(TAG, "start SignUpActivity.");
            Intent intent = new Intent(this, SignUpActivity.class);
            startActivity(intent);
        }
    }


    AuthorizationContract.SignIn forwardInteraction;

    private void setPresenter(AuthorizationContract.SignIn presenter){
        this.forwardInteraction = presenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);
        ButterKnife.bind(this);

        AuthorizationPresenter presenter = new AuthorizationPresenter(this);
        setPresenter(presenter);
    }


    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void doIt() {

    }
}
