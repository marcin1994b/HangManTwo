package com.marcinbaranski.hangmantwo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

import com.marcinbaranski.hangmantwo.Manifest;
import com.marcinbaranski.hangmantwo.R;
import com.marcinbaranski.hangmantwo.presenters.AuthorizationContract;
import com.marcinbaranski.hangmantwo.presenters.AuthorizationPresenter;

public class MainActivity extends RuntimePermissionsActivity implements AuthorizationContract.PublishToView {


    AuthorizationContract.getAuth forwardInteraction;
    int REQUEST_PERMISSIONS = 2;

    private void setPresenter(AuthorizationContract.getAuth presenter){
        this.forwardInteraction = presenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        AuthorizationPresenter presenter = new AuthorizationPresenter(this);
        setPresenter(presenter);

        this.requestAppPermissions(new
                String[]{Manifest.permission.INTERNET}, R.string.app_name, REQUEST_PERMISSIONS);



    }

    @Override
    public void onPermissionsGranted(int requestCode) {
        forwardInteraction.getAuthInstance();
        this.onResume();
    }

    @Override
    public void onPermissionsNotGranted(int requestCode) {
        Toast.makeText(this, "We need Internet connection to authentication.", Toast.LENGTH_LONG).show();
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void doIt() {
        Intent intent = new Intent(this, SignInActivity.class);
        startActivity(intent);
    }
}
