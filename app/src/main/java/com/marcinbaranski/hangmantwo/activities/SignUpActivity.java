package com.marcinbaranski.hangmantwo.activities;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import com.marcinbaranski.hangmantwo.R;
import com.marcinbaranski.hangmantwo.presenters.AuthorizationContract;
import com.marcinbaranski.hangmantwo.presenters.AuthorizationPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class SignUpActivity extends AppCompatActivity implements AuthorizationContract.PublishToView{

    @BindView(R.id.editTextNick)
    EditText nickEditText;
    @BindView(R.id.editTextEmail)
    EditText emailEditText;
    @BindView(R.id.editTextPasswordFirst)
    EditText passwordFirstEditText;
    @BindView(R.id.editTextPasswordSecond)
    EditText passwordSecondEditText;
    @BindView(R.id.buttonSignUp)
    Button signUpButton;
    @BindView(R.id.textViewSignIn)
    TextView signInTextView;

    @OnClick({R.id.textViewSignIn, R.id.buttonSignUp})
    public void onClick(View v) {
        if(v == signUpButton){
            forwardInteraction.onSignUpButtonClick(this, nickEditText.getText().toString().trim(),
                    emailEditText.getText().toString().trim(),
                    passwordFirstEditText.getText().toString().trim(),
                    passwordSecondEditText.getText().toString().trim());

        }else if(v == signInTextView){
            Intent intent = new Intent(this, SignInActivity.class);
            startActivity(intent);
        }
    }

    AuthorizationContract.SignUp forwardInteraction;

    private void setPresenter(AuthorizationContract.SignUp presenter){
        this.forwardInteraction = presenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_up);
        ButterKnife.bind(this);

        AuthorizationPresenter presenter = new AuthorizationPresenter(this);
        setPresenter(presenter);
    }

    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void doIt() {
        Intent intent = new Intent(this, MainActivity.class);
        startActivity(intent);
    }
}
