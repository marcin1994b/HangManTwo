package com.marcinbaranski.hangmantwo.activities;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.marcinbaranski.hangmantwo.R;
import com.marcinbaranski.hangmantwo.presenters.AuthorizationContract;
import com.marcinbaranski.hangmantwo.presenters.AuthorizationPresenter;

import butterknife.BindView;
import butterknife.ButterKnife;
import butterknife.OnClick;

public class RemindPasswordActivity extends AppCompatActivity implements AuthorizationContract.PublishToView{

    @BindView(R.id.editTextEmail)
    EditText emailEditText;
    @BindView(R.id.buttonRemindPassword)
    Button remindPasswordButton;

    @OnClick({R.id.buttonRemindPassword})
    public void onClick(View v) {
        if(v == remindPasswordButton){
            forwardInteraction.onRemindButtonClick(this, emailEditText.getText().toString().trim());
        }
    }

    AuthorizationContract.RemindPassword forwardInteraction;

    private void setPresenter(AuthorizationContract.RemindPassword presenter){
        this.forwardInteraction = presenter;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_remind_password);
        ButterKnife.bind(this);

        AuthorizationPresenter presenter = new AuthorizationPresenter(this);
        setPresenter(presenter);
    }


    @Override
    public void showToastMessage(String message) {
        Toast.makeText(this, message, Toast.LENGTH_SHORT).show();
    }

    @Override
    public void doIt() {
        Toast.makeText(this, "Email withm password restart send.", Toast.LENGTH_SHORT).show();
        finish();
    }
}
