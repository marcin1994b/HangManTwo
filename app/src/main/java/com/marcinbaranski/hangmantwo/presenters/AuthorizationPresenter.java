package com.marcinbaranski.hangmantwo.presenters;

import android.app.Activity;

import com.marcinbaranski.hangmantwo.models.Authorization;

/**
 * Created by Marcin on 17.06.2017.
 */

public class AuthorizationPresenter implements AuthorizationContract.SignIn, AuthorizationContract.SignUp,
        AuthorizationContract.RemindPassword, Authorization.AuthorizationResult, AuthorizationContract.getAuth {

    AuthorizationContract.PublishToView publishResults;
    Authorization auth;

    public AuthorizationPresenter(AuthorizationContract.PublishToView publishResults){
        this.publishResults = publishResults;
        auth = new Authorization();
        auth.setAuthorizationResultListener(this);
    }

    @Override
    public void onSignInButtonClick(Activity activity, String email, String password) {
        auth.login(activity, email, password);
    }

    @Override
    public void onSignUpButtonClick(Activity activity,  String nick, String email, String passwordFirst, String passwordSecond) {
        auth.createNewAccount(activity, nick, email, passwordFirst, passwordSecond);
    }

    @Override
    public void onRemindButtonClick(Activity activity, String email) {
        auth.remindPassword(activity, email);
    }

    @Override
    public void onAuthResult(String message, boolean successful) {
        if(successful){
            publishResults.doIt();
        }else{
            publishResults.showToastMessage(message);
        }
    }

    @Override
    public void getAuthInstance() {
        auth.isSignedIn();
    }


    @Override
    public void onGetAuthInstanceResult(String tmp, boolean successful) {
        if(successful){
            publishResults.showToastMessage(tmp);
        }else{
            publishResults.doIt();
        }
    }
}
