package com.marcinbaranski.hangmantwo.presenters;

import android.app.Activity;

/**
 * Created by Marcin on 17.06.2017.
 */

public interface AuthorizationContract {

    public interface SignIn{

        public void onSignInButtonClick (Activity activity, String email, String password);

    }

    public interface SignUp{

        public void onSignUpButtonClick(Activity activity,  String nick, String email, String passwordFirst, String passwordSecond);

    }

    public interface RemindPassword{

        public void onRemindButtonClick(Activity activity, String email);

    }


    public interface PublishToView{

        public void showToastMessage(String message);

        public void doIt();

    }

    public interface getAuth{

        public void getAuthInstance();
    }

}
