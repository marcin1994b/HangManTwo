package com.marcinbaranski.hangmantwo.models;

import android.app.Activity;
import android.support.annotation.NonNull;

import com.google.android.gms.tasks.OnCompleteListener;
import com.google.android.gms.tasks.Task;
import com.google.firebase.auth.AuthResult;
import com.google.firebase.auth.FirebaseAuth;
import com.google.firebase.auth.FirebaseUser;

/**
 * Created by Marcin on 17.06.2017.
 */

public class Authorization {

    private Authorization.AuthorizationResult authorizationListener;
    private String authorizationMessage;


    private FirebaseAuth mAuth;
    private FirebaseUser currentUser;

    public interface AuthorizationResult{
        void onAuthResult(String message, boolean successful);

        void onGetAuthInstanceResult(String tmp, boolean successful);
    }

    public Authorization(){
        mAuth = FirebaseAuth.getInstance();
    }

    public void setAuthorizationResultListener(Authorization.AuthorizationResult listener){
        this.authorizationListener = listener;
        authorizationMessage = "";
    }

    public void createNewAccount(Activity activity, String nick, String email, String passwordFirst, String passwordSecond){
        System.out.println("Tworzenie konta...");
        String err = "";
        if(nick.length() < 4){
            System.out.println("nick jest za krotki.");
            err = "Your nick is too short. ";
            authorizationListener.onAuthResult(err, false);
        }
        if(passwordFirst.length() < 4){
            System.out.println("haslo jest za krotkie");
            err = "Your password is too short. ";
            authorizationListener.onAuthResult(err, false);
        }
        if(!passwordFirst.equals(passwordSecond)){
            err = "Passwords are different. ";
            authorizationListener.onAuthResult(err, false);
        }
        if(err.length() == 0){
            System.out.println("Autoryzacja...");
            mAuth.createUserWithEmailAndPassword(email, passwordFirst).
                    addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
                @Override
                public void onComplete(@NonNull Task<AuthResult> task) {
                    if(task.isSuccessful()){
                        System.out.println("Sukces.");
                        authorizationListener.onAuthResult("", true);
                    }else{
                        System.out.println("Porazka");
                        authorizationListener.onAuthResult("Authentication failed.", false);
                    }
                }
            });
        }
    }

    public void remindPassword(Activity activity, String email){
        mAuth.sendPasswordResetEmail(email).addOnCompleteListener(activity, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                if(task.isSuccessful()){
                    authorizationListener.onAuthResult("", true);
                }else{
                    authorizationListener.onAuthResult("Password reset faild.", false);
                }
            }
        });
    }

    public void login(Activity activity, String email, String password){
        mAuth.signInWithEmailAndPassword(email, password).addOnCompleteListener(activity, new OnCompleteListener<AuthResult>() {
            @Override
            public void onComplete(@NonNull Task<AuthResult> task) {
                if(task.isSuccessful()){
                    authorizationListener.onAuthResult("", true);
                }else{
                    authorizationListener.onAuthResult("Invalid email or password.", false);
                }
            }
        });
        currentUser.sendEmailVerification().addOnCompleteListener(activity, new OnCompleteListener<Void>() {
            @Override
            public void onComplete(@NonNull Task<Void> task) {
                
            }
        });
    }

    public void isSignedIn(){
        currentUser = mAuth.getCurrentUser();
        if(currentUser == null){
            authorizationListener.onAuthResult("", true);
        }else{
            authorizationListener.onAuthResult("Welcom " + currentUser.getEmail(), false);
        }
    }
}
